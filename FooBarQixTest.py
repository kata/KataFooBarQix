import unittest


from FooBarQix import FooBarQix

class MyTestCase(unittest.TestCase):
    def test_False(self):
        self.assertTrue(self)

    def test_0_raises_value_error(self):
        with self.assertRaises(ValueError):
            FooBarQix(0)

    def test_1_not_raise_value_error(self):
        assert FooBarQix(1).number == 1

    def test_minus_1_raises_value_error(self):
        with self.assertRaises(ValueError):
            FooBarQix(-1)

    def test_1_return_1(self):
        assert FooBarQix(1).result == 1

    def test_2_return_2(self):
        assert FooBarQix(2).result == 2

    def test_9_return_Foo(self):
        assert FooBarQix(9).result == "Foo"

    def test_10_return_Bar(self):
        assert FooBarQix(10).result == "Bar"

    def test_14_return_Qix(self):
        assert FooBarQix(14).result == "Qix"

    def test_3_return_FooFoo(self):
        assert FooBarQix(3).result == "FooFoo"

    def test_5_return_BarBar(self):
        assert FooBarQix(5).result == "BarBar"

    def test_7_return_QixQix(self):
        assert FooBarQix(7).result == "QixQix"

    def test_13_return_Foo(self):
        assert FooBarQix(13).result == "Foo"

    def test_15_return_FooBarBar(self):
        assert FooBarQix(15).result == "FooBarBar"

    def test_21_return_FooQix(self):
        assert FooBarQix(21).result == "FooQix"

    def test_33_return_FooFooFoo(self):
        assert FooBarQix(33).result == "FooFooFoo"

    def test_53_return_BarFoo(self):
        assert FooBarQix(53).result == "BarFoo"


if __name__ == '__main__':
    unittest.main()

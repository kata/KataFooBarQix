class FooBarQix(object):
    def __init__(self, number):
        if self.is_valid_number(number):
            self.number = number
            self.rules = [(3, "Foo"), (5, "Bar"), (7, "Qix"), (0, "*")]

    def compute(self):
        result = self.calculate_mod()

        for digit in map(int, str(self.number)):
            for rule in self.rules:
                if rule[0] == digit:
                    result += rule[1]
        return result if result else self.number

    def calculate_mod(self):
        result = ""
        for rule in self.rules:
            if rule[0] != 0 and self.number % rule[0] == 0:
                result += rule[1]
        return result

    def is_valid_number(self, number):
        if number <= 0:
            raise ValueError("Number must be positive")
        return True


for num in range(1, 10102):
    print(num, ' ==> ', FooBarQix(num).compute())
    # print(num,FooBarQix(num).result())
